package flashcopy_workrate.controller;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;

import flashcopy_workrate.Main;
import flashcopy_workrate.model.Machine;
import flashcopy_workrate.model.People;
import flashcopy_workrate.model.PeopleWorkRate;
import flashcopy_workrate.model.Project;
import flashcopy_workrate.model.dbaccess.WorkRateAccess;

public class WorkRateController {
	public static PeopleWorkRate getLastWorkRate(People people)
	{
		return WorkRateAccess.getLastWorkRate(people);
	}
	public static boolean createWorkRate(People people, Machine machine, Project project,String date, String comment, String workRate)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date dateObj;
		try {
			dateObj = sdf.parse(date);
			java.sql.Date sqlStartDate = new java.sql.Date(dateObj.getTime()); 
			int rate = -1;
			if(!"".equals(workRate))
				rate = Integer.parseInt(workRate);
			PeopleWorkRate workrate = new PeopleWorkRate(people,sqlStartDate,comment,rate, machine,project);
			return WorkRateAccess.createWorkRate(workrate);
		} catch (ParseException e) {
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
			return false;
		}
		
	}
	public static boolean updateWorkRate(People people, Machine machine, Project project,String date, String comment, String workRate)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date dateObj;
		try {
			dateObj = sdf.parse(date);
			java.sql.Date sqlStartDate = new java.sql.Date(dateObj.getTime()); 			
			PeopleWorkRate workrate = new PeopleWorkRate(people,sqlStartDate,comment,Integer.parseInt(workRate), machine,project);
			return WorkRateAccess.updateWorkRate(workrate);
		} catch (ParseException e) {
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
			return false;
		}
	}
	public static boolean deleteWorkRate(String date, int people, String project, String machine)
	{
		return WorkRateAccess.deleteWorkRate(people, machine, project, date);
	}
	public static ArrayList<PeopleWorkRate> getWorkRates(int peopleID, int month, int year)
	{
		String firstDay = getFirstDay(1,month, year);
        String lastDay = getLastDay(1, month, year);
		return WorkRateAccess.getWorkRates(peopleID,firstDay, lastDay);
	}
	private static String getFirstDay(int day, int month, int year)
	{
		String dayString;
		String monthString = ""+month;
		if(month < 10)
			monthString = "0"+month;
		
		if(day < 10)
			dayString = year+"-"+monthString+"-"+"0"+ day;
		else 
			dayString = year+"-"+monthString+"-"+day;
		
		Calendar cal = Calendar.getInstance();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
			cal.setTime(df.parse(dayString));
		} catch (ParseException e) {
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
        if(2 == cal.get(Calendar.DAY_OF_WEEK))
        	return dayString;
        else {
        	day += 1;
			return getFirstDay(day,  month, year);
		}
	}
	private static String getLastDay(int day, int month, int year)
	{
		String monthString = ""+month;
		if(month < 10)
			monthString = "0"+month;
		String lastDayString = "";
		Calendar cal = Calendar.getInstance();
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        try {
			cal.setTime(df.parse(day+"-"+month+"-"+year));
		} catch (ParseException e) {
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
	    int lastDate = cal.getActualMaximum(Calendar.DATE);
	    cal.set(Calendar.DATE, lastDate);
	    int lastDay = cal.get(Calendar.DAY_OF_WEEK);
	    if(lastDay == 7 )
	    {
	    	lastDate -= 1; 
	    	lastDayString = year+"-"+monthString+"-"+lastDate;
	    }
	    else if(lastDay == 1 )
	    {
	    	lastDate -= 2; 
	    	lastDayString = year+"-"+monthString+"-"+lastDate;
	    }
	    else if(lastDay == 6)
	    	lastDayString = year+"-"+monthString+"-"+lastDate;
	    else {
			int nextDay = 6-lastDay;
			int nextMonth = month + 1;
			int nextYear = year;
			if(nextMonth > 12)
			{
				nextMonth %= 12;
				nextYear +=1;
			}
			String nextString = ""+nextMonth;
			if(nextMonth < 10)
				nextString = "0"+nextMonth;
			
			if(nextDay <10)
				lastDayString = nextYear+"-"+nextString+"-"+"0"+nextDay;
			else 
				lastDayString = nextYear+"-"+nextString+"-"+nextDay;
		}
	    return lastDayString;
	}
	public static PeopleWorkRate getWorkRate(Date date, int people, String project, String machine)
	{
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = formatter.format(date);
		return WorkRateAccess.getWorkRate(dateString, people, project, machine);
	}
}
