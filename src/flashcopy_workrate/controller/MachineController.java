package flashcopy_workrate.controller;

import java.util.ArrayList;

import flashcopy_workrate.model.Machine;
import flashcopy_workrate.model.People;
import flashcopy_workrate.model.PeopleWorkRate;
import flashcopy_workrate.model.Project;
import flashcopy_workrate.model.dbaccess.MachineAccess;
import flashcopy_workrate.model.dbaccess.WorkRateAccess;

public class MachineController {
	public static ArrayList<Machine> getMachines()
	{
		return MachineAccess.getMachines();
	}
	public static boolean createMachine(Machine m)
	{
		return MachineAccess.createMachine(m);
	}
	public static String[] getMachinesToArray()
	{
		String[] res = null;
		ArrayList<Machine> machines = getMachines();
		if(!machines.isEmpty())
		{
			res = new String[machines.size()];
			for(int i = 0 ; i < machines.size() ; i++)
			{
				res[i] = machines.get(i).getName();
			}
		}
		return res;
	}
	public static Machine getCurrentMachine(People p)
	{
		PeopleWorkRate rate = WorkRateAccess.getLastWorkRate(p);
		if(rate == null)
			return null;
		else
			return rate.getMachine();
	}
	public static boolean deleteMachine(String name)
	{
		return MachineAccess.deleteMachine(name);
	}
}
