package flashcopy_workrate.controller;

import java.sql.Date;
import java.util.ArrayList;

import flashcopy_workrate.model.People;
import flashcopy_workrate.model.Project;
import flashcopy_workrate.model.dbaccess.PeopleAccess;

public class PeopleController {
	public static ArrayList<String> getPeopleString()
	{
		ArrayList<String> result = new ArrayList<String>();
		ArrayList<People> peoples = PeopleAccess.getPeoples();
		if(peoples.size() > 0)
		{
			for(People people : peoples)
			{
				result.add(people.getFirstName() + " "+people.getLastName());
			}
		}
		return result;
	}
	public static People getPeopleByID(int id)
	{
		return PeopleAccess.getPeopleById(id);
	}
	public static ArrayList<People> getPeoples()
	{
		return PeopleAccess.getPeoples();
	}
	public static String[] getPeopleToArray()
	{	
		ArrayList<String> res = new ArrayList();
		for(People p : getPeoples())
		{
			res.add(p.getFullName());
		}
		return (String[]) res.toArray(new String[res.size()]);
	}
	public static boolean createPeople(People people)
	{
		String firstname = people.getFirstName();
		String lastname = people.getLastName();		
		firstname = firstname.replace("'","''");
		lastname = lastname.replace("'", "''");	
		People p = new People(firstname, lastname, people.getHireDate());
		Boolean hasBeenCreated = PeopleAccess.createPeople(p);
		return hasBeenCreated;
	}
	public static boolean deletePeople(int id)
	{
		return PeopleAccess.deletePeople(id);
	}
	public static boolean editPeople(People p)
	{
		boolean res;
		String firstName = p.getFirstName();
		String lastName = p.getLastName();
		String date = p.getStringHireDate();
		res = PeopleAccess.editPeople(p.getId(),firstName, lastName, date);
		return res;
	}
}
