package flashcopy_workrate.controller;

import java.util.ArrayList;

import flashcopy_workrate.model.People;
import flashcopy_workrate.model.PeopleWorkRate;
import flashcopy_workrate.model.Project;
import flashcopy_workrate.model.dbaccess.ProjectAccess;

public class ProjectController {
	public static ArrayList<Project> getProjects()
	{
		return ProjectAccess.getProjects();
	}
	public static boolean createProject(Project p)
	{
		return ProjectAccess.createProject(p);
	}
	public static String[] getProjectsArray()
	{
		String[] res = null;
		ArrayList<Project> projects = getProjects();
		if(!projects.isEmpty())
		{
			res = new String[projects.size()];
			for(int i = 0 ; i < projects.size() ; i++)
			{
				res[i] = projects.get(i).getName();
			}
		}
		return res;
	}
	public static Project getPeopleCurrentProject(People people)
	{
		PeopleWorkRate rate = WorkRateController.getLastWorkRate(people);
		if(rate == null)
			return null;
		return rate.getCurrentProject();
	}
	public static boolean deleteProject(String projectName)
	{
		return ProjectAccess.deleteProject(projectName);
	}
	public static Project getProjectByName(String name)
	{
		return ProjectAccess.getProject(name);
	}
}
