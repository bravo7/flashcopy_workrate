package flashcopy_workrate.model;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class PeopleWorkRate {
	private People people;
	private Date workRateDate;
	private String comment;
	private int rate;
	private Machine machine;
	private Project currentProject;
	
	public PeopleWorkRate(People people, Date workRateDate, String comment,int rate, Machine machine, Project currentProject)
	{
		this.currentProject = currentProject;
		this.people = people;
		this.workRateDate = workRateDate;
		this.comment = comment;
		this.rate = rate;
		this.machine = machine;
	}
	
	public People getPeople()
	{
		return this.people;
	}
	public void setPeople(People value)
	{
		this.people = value;
	}
	public Date getWorkRateDate()
	{
		return this.workRateDate;
	}
	public void setWorkRateDate(Date value)
	{
		this.workRateDate= value;
	}
	public String getStringDate()
	{
		 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");  
         return dateFormat.format(this.workRateDate);  
	}
	public String getComment()
	{
		return this.comment;
	}
	public void setComment(String value)
	{
		this.comment = value;
	}
	public int getRate()
	{
		return this.rate;
	}
	public void setRate(int value)
	{
		this.rate = value;
	}
	public Machine getMachine()
	{
		return this.machine;
	}
	public void setMachine(Machine value)
	{
		this.machine = value;
	}
	public Project getCurrentProject()
	{
		return this.currentProject;
	}
	public void setCurrentProject(Project value)
	{
		this.currentProject = value;
	}
	
	@Override
	public String toString()
	{
		return this.getStringDate()
				+ "/"+this.getRate()
				+ "/"+this.getComment()
				+ "/"+this.getPeople().getId()
				+ "/"+this.getMachine().getName()
				+ "/"+this.getCurrentProject().getName();
	}
	
}
