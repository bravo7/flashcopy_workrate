package flashcopy_workrate.model;

public class Machine {
	private String name;
	
	public Machine(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void setName(String value)
	{
		this.name = value;
	}
	@Override 
	public String toString()
	{
		return this.getName();
	}
}
