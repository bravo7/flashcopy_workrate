package flashcopy_workrate.model.dbaccess;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;

import flashcopy_workrate.Main;
import flashcopy_workrate.controller.PeopleController;
import flashcopy_workrate.model.Machine;
import flashcopy_workrate.model.People;
import flashcopy_workrate.model.PeopleWorkRate;
import flashcopy_workrate.model.Project;
import flashcopy_workrate.model.dbaccess.utils.DBConnectionWorkRate;

public class WorkRateAccess {
	public static boolean createWorkRate(PeopleWorkRate workrate)
	{
		String res = "INSERT INTO workrate (date, rate, comment,machine_name,people_id,current_project) "
				+ "VALUES ("
				+ "'"+workrate.getStringDate()+"',"+workrate.getRate()+",'"+workrate.getComment()+"',"
				+ "'"+workrate.getMachine().getName()+"',"+workrate.getPeople().getId()+ ",'"+ workrate.getCurrentProject().getName() +"');";
		try {
			int result = (int)DBConnectionWorkRate.doRequest(res);
			if(result == 0)
				return false;
			else
				return true;
		} catch (SQLException e) {
			//TODO : pop error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
			return false;
		}
	}
	public static boolean updateWorkRate(PeopleWorkRate workrate)
	{
		String res = "UPDATE workrate set comment='"+workrate.getComment()+"', rate='"+workrate.getRate()+"'"
				+ "WHERE people_id = "+workrate.getPeople().getId()+" AND date = '"+workrate.getStringDate()+"' AND current_project = '"+workrate.getCurrentProject().getName()+"' AND machine_name='"+workrate.getMachine().getName()+"';";
		try {
			int result = (int)DBConnectionWorkRate.doRequest(res);
			if(result == 0)
				return false;
			else
				return true;
		} catch (SQLException e) {
			//TODO : pop error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
			return false;
		}
	}
	public static boolean deleteWorkRate(int people, String machine, String project, String date)
	{
		String req = "DELETE FROM workrate WHERE people_id = "+people+" AND date = '"+date+"' AND current_project = '"+project+"' AND machine_name='"+machine+"';";
		try {
			int result = (int)DBConnectionWorkRate.doRequest(req);
			if(result == 0)
				return false;
			else
				return true;
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
			return false;
		}
	}
	public static ArrayList<PeopleWorkRate> getWorkRates()
	{
		ArrayList<PeopleWorkRate> resultList = new ArrayList<PeopleWorkRate>();
		String req = "SELECT * FROM workrate;";
		try {
			ResultSet result = (ResultSet) DBConnectionWorkRate.doRequest(req);
			while (result.next()) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				
				java.util.Date date = format.parse ( result.getString("date")); 
				String comment = result.getString("comment");
				int rate = result.getInt("rate");
				String machineName = result.getString("machine_name");
				int peopleId = result.getInt("people_id");
				People people = PeopleController.getPeopleByID(peopleId);
				PeopleWorkRate w = new PeopleWorkRate(people,new java.sql.Date(date.getTime()),comment,rate, new Machine(machineName),null);
				resultList.add(w);
			}			
			
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
		catch (ParseException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
		return resultList;
	}
	public static PeopleWorkRate getLastWorkRate(People people)
	{
		PeopleWorkRate res = null;
		String req = "SELECT * FROM workrate where people_id = "+people.getId()+" ORDER BY date DESC LIMIT 1 ;";
		try {
			ResultSet result = (ResultSet) DBConnectionWorkRate.doRequest(req);
			while (result.next()) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				
				java.util.Date date = format.parse ( result.getString("date")); 
				String comment = result.getString("comment");
				int rate = result.getInt("rate");
				String machineName = result.getString("machine_name");
				Project project = ProjectAccess.getProject(result.getString("current_project"));
				res = new PeopleWorkRate(people,new java.sql.Date(date.getTime()),comment,rate, new Machine(machineName),project);
			}			
			
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
		catch (ParseException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
		return res;
	}
	public static PeopleWorkRate getWorkRate(String date, int peopleID, String projectName, String machineName)
	{
		PeopleWorkRate res = null;
		String req = "SELECT * FROM workrate WHERE people_id = "+peopleID+" AND date = '"+date+"' AND current_project = '"+projectName+"' AND machine_name='"+machineName+"';";
		try {
			ResultSet result = (ResultSet) DBConnectionWorkRate.doRequest(req);
			while (result.next()) {
				People people = PeopleAccess.getPeopleById(peopleID);
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");				
				java.util.Date dateObj = format.parse ( result.getString("date")); 
				String comment = result.getString("comment");
				if(comment == null)
					comment = "";
				int rate = result.getInt("rate");
				Machine machine = new Machine(result.getString("machine_name"));
				Project project = ProjectAccess.getProject(result.getString("current_project"));
				res = new PeopleWorkRate(people,new java.sql.Date(dateObj.getTime()),comment,rate, machine,project);
			}			
			
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
		catch (ParseException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
		return res;
	}
	public static ArrayList<PeopleWorkRate> getWorkRates(int peopleID, String firstDay, String lastDay)
	{
		ArrayList<PeopleWorkRate> res = new ArrayList<PeopleWorkRate>();
		String req = "SELECT * FROM workrate WHERE people_id = "+peopleID+""
				+ " AND date BETWEEN '"+firstDay+"' AND '"+lastDay+"'"
				+ " ORDER BY date; ";
		
		try
		{
			ResultSet result = (ResultSet) DBConnectionWorkRate.doRequest(req);
			while (result.next()) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");				
				java.util.Date datefi = format.parse ( result.getString("date")); 
				String comment = result.getString("comment");
				int rate = result.getInt("rate");
				String machineName = result.getString("machine_name");
				int peopleId = result.getInt("people_id");
				People people = PeopleController.getPeopleByID(peopleId);
				Project project = ProjectAccess.getProject(result.getString("current_project"));
				PeopleWorkRate w = new PeopleWorkRate(people,new java.sql.Date(datefi.getTime()),comment,rate, new Machine(machineName),project);
				res.add(w);
			}
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
		catch (ParseException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
		
		return res;
	}
}
