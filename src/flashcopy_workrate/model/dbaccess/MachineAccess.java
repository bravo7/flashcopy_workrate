package flashcopy_workrate.model.dbaccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;

import flashcopy_workrate.Main;
import flashcopy_workrate.model.Machine;
import flashcopy_workrate.model.dbaccess.utils.DBConnectionWorkRate;

public class MachineAccess {
	public static boolean createMachine(Machine machine)
	{
		String res = "INSERT INTO machine (name) VALUES ('"+machine.getName()+"');";
		try {
			int result = (int)DBConnectionWorkRate.doRequest(res);
			if(result == 0)
				return false;
			else
				return true;
		} catch (SQLException e) {
			//TODO : pop error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
			return false;
		}
	}
	public static ArrayList<Machine> getMachines()
	{
		ArrayList<Machine> resultList = new ArrayList<Machine>();
		String req = "SELECT * FROM machine;";
		try {
			ResultSet result = (ResultSet) DBConnectionWorkRate.doRequest(req);
			while (result.next()) {
				resultList.add(new Machine(result.getString("name")));
			}			
			
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
		return resultList;
	}
	public static boolean deleteMachine(String machine)
	{
		String req = "DELETE FROM machine where name='"+machine+"'";
		try {
			int result = (int)DBConnectionWorkRate.doRequest(req);
			if(result == 0)
				return false;
			else
				return true;
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
			return false;
		}
	}
}
