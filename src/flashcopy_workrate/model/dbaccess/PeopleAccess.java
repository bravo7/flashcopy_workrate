package flashcopy_workrate.model.dbaccess;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;

import flashcopy_workrate.Main;
import flashcopy_workrate.model.Machine;
import flashcopy_workrate.model.People;
import flashcopy_workrate.model.Project;
import flashcopy_workrate.model.dbaccess.utils.DBConnectionWorkRate;

public class PeopleAccess {
	public static boolean createPeople(People people)
	{
		String req = "INSERT INTO people (firstname,lastname, hiredate) VALUES "
				+ "('"+people.getFirstName()+"','"+people.getLastName()+"',"
				+ "'"+people.getStringHireDate()+"'); ";
		try {
			int result = (int) DBConnectionWorkRate.doRequest(req);
			if(result == 0)
				return false;
			else
				return true;
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
			return false;
		}
	}
	public static ArrayList<People> getPeoples()
	{
		ArrayList<People> resultList = new ArrayList<People>();
		String req = "SELECT * FROM people";
		try {
			ResultSet result = (ResultSet) DBConnectionWorkRate.doRequest(req);
			while (result.next()) {
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				java.util.Date date = format.parse ( result.getString("hiredate")); 
				People p = new People(result.getString("firstname"), result.getString("lastname"),new Date(date.getTime()));
				p.setId(result.getInt("id"));				
				resultList.add(p);
			}			
			
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
		catch (ParseException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
		return resultList;
	}
	public static boolean deletePeople(int id)
	{
		String req = "DELETE FROM people where id="+id+";";
		try {
			int result = (int)DBConnectionWorkRate.doRequest(req);
			if(result == 0)
				return false;
			else
				return true;
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
			return false;
		}
	}
	public static People getPeopleById(int id)
	{
		String req = "SELECT * FROM people where id = "+id;
		People res = null;
		try {
			ResultSet result = (ResultSet) DBConnectionWorkRate.doRequest(req);
			while (result.next()) {
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				java.util.Date date = format.parse ( result.getString("hiredate")); 
				People p = new People(result.getString("firstname"), result.getString("lastname"),new Date(date.getTime()));
				p.setId(result.getInt("id"));
				res = p;
			}			
			
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
		catch (ParseException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
		return res;
	}
	public static boolean editPeople(int id, String firstName, String lastName, String date)
	{
		String req = "UPDATE people set firstname='"+firstName+"',lastname='"+lastName+"',hiredate = '"+date+"'"
				+ "where id = "+id;
		try {
			int result = (int)DBConnectionWorkRate.doRequest(req);
			if(result == 0)
				return false;
			else
				return true;
		} catch (SQLException e) {
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
			return false;
		}
	}
}
