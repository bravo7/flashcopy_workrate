package flashcopy_workrate.model.dbaccess.utils;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;

import flashcopy_workrate.Main;

public class DBConnectionWorkRate {
	
	//properties
	private static String url = "jdbc:sqlite:"+System.getProperty("user.dir")+"/db/WorkRate.db";
	private static Connection conn = null;
	
	//methods
	public static void establishConnection() throws SQLException, IOException
	{
		Main.LOGGER.log(Level.SEVERE,"Creating Connection");
		createDataBaseFile();
		if(conn == null)
			conn = DriverManager.getConnection(DBConnectionWorkRate.url);
	}
	public static void closeConnection() throws SQLException
	{
		Main.LOGGER.log(Level.SEVERE,"Closing Connection");
		if (conn != null)
			 conn.close();
	}
	public static Object doRequest(String req) throws SQLException
	{
		Main.LOGGER.log(Level.SEVERE,req);
		
		Object res = null;
		Statement stmt = conn.createStatement();
		if(req.startsWith("SELECT"))
			res = stmt.executeQuery(req);
		else if(req.startsWith("UPDATE") || req.startsWith("DELETE") || req.startsWith("INSERT"))
			res = stmt.executeUpdate(req);
		else
			res = stmt.execute(req);
		return res;
	}
	
	private static void createDataBaseFile() throws IOException, SQLException
	{
		String folderPath = System.getProperty("user.dir")+"/db/";
		File folder = new File(folderPath);
		if(!folder.exists())
			folder.mkdirs();
		
		File file = new File(folderPath+"WorkRate.db");
		if(!file.exists())
		{
			file.createNewFile();
			createDataBase();
		}
	}
	private static void createDataBase() throws SQLException
	{
		if(conn == null)
			conn = DriverManager.getConnection(DBConnectionWorkRate.url);
		Statement stmt = conn.createStatement();
		try
		{
			stmt.execute("CREATE TABLE machine(name varchar NOT NULL PRIMARY KEY)");
			
			stmt.execute("CREATE TABLE project(name varchar NOT NULL PRIMARY KEY, average_workrate int NOT NULL)");
			
			stmt.execute("CREATE TABLE people(id integer  PRIMARY KEY AUTOINCREMENT, firstname varchar NOT NULL,"
					+ "lastname varchar NOT NULL, project_name varchar , hiredate date NOT NULL )");	
			
			stmt.execute("CREATE TABLE workrate ("
					+ "date date NOT NULL,"
					+ "rate integer,"
					+ "comment varchar,"
					+ "machine_name varchar NOT NULL,"
					+ "people_id int NOT NULL,"
					+ "current_project string NOT NULL,"
					+ "PRIMARY KEY (date, machine_name,current_project,people_id)"
					+ ")");
		}
		catch(SQLException e)
		{
			throw e;
		}
		finally
		{
			stmt.close();
		}
		
	}
}
