package flashcopy_workrate.model.dbaccess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;

import flashcopy_workrate.Main;
import flashcopy_workrate.model.Machine;
import flashcopy_workrate.model.People;
import flashcopy_workrate.model.Project;
import flashcopy_workrate.model.dbaccess.utils.DBConnectionWorkRate;

public class ProjectAccess {
	public static boolean createProject(Project project)
	{
		String req = "INSERT INTO project (name, average_workrate) VALUES ('"+project.getName()+"',"+project.getAverageWorkRate()+");";
		try {
			int result = (int) DBConnectionWorkRate.doRequest(req);
			if(result == 0)
				return false;
			else
				return true;
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
			return false;
		}
	}
	public static ArrayList<Project> getProjects()
	{
		ArrayList<Project> resultList = new ArrayList<>();
		String req = "SELECT * FROM project";
		try {
			ResultSet result = (ResultSet) DBConnectionWorkRate.doRequest(req);
			while (result.next()) {
				resultList.add(new Project(result.getString("name"), result.getInt("average_workrate")));
			}	
		}
		catch(SQLException e)
		{
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
		return resultList;
	}
	public static Project getProject(String name)
	{
		Project res = null;
		String req = "SELECT * FROM project where name='"+name+"';";
		try {
			ResultSet result = (ResultSet) DBConnectionWorkRate.doRequest(req);
			while (result.next()) {
				res = new Project(result.getString("name"), result.getInt("average_workrate"));
			}
		}
		catch(SQLException e)
		{
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
		return res;
	}
	public static boolean deleteProject(String project)
	{
		String req = "DELETE FROM project where name='"+project+"'";
		try {
			int result = (int)DBConnectionWorkRate.doRequest(req);
			if(result == 0)
				return false;
			else
				return true;
		} catch (SQLException e) {
			//TODO : pop up error
			Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
			return false;
		}
	}
}
