package flashcopy_workrate.model;

public class Project {
	private String name;
	private int averageWorkRate;
	
	public Project(String name, int averageWorkRate)
	{
		this.name = name;
		this.averageWorkRate = averageWorkRate;
	}
	
	public String getName()
	{
		return this.name;
	}
	public int getAverageWorkRate()
	{
		return this.averageWorkRate;
	}
	
	public void setName(String value)
	{
		this.name = value;
	}
	public void setAverageWorkRate(int value)
	{
		this.averageWorkRate = value;
	}
	
	@Override
	public String toString()
	{
		return ""+this.getName()+"/"+this.getAverageWorkRate();
	}
}
