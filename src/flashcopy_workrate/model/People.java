package flashcopy_workrate.model;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class People {
	private int id;
	private String firstName;
	private String lastName;
	private Date hireDate;
	
	public People(String firstName, String lastName, Date hireDate)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.hireDate = hireDate;
	}
	
	public int getId()
	{
		return this.id;
	}
	public String getFirstName()
	{
		return this.firstName;
	}
	public String getLastName()
	{
		return this.lastName;
	}
	public Date getHireDate()
	{
		return this.hireDate;
	}	
	public String getStringHireDate()
	{
		 DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");  
         return dateFormat.format(this.hireDate);  
	}
	public void setId(int value)
	{
		this.id = value;
	}
	public void setLastName(String value)
	{
		this.lastName = value;
	}
	public void setFirstName(String value)
	{
		this.firstName = value;
	}
	public void setHireDate(Date value)
	{
		this.hireDate = value;
	}
	public String getFullName()
	{
		return this.getFirstName() + " " +this.getLastName();
	}
	@Override
	public String toString()
	{
		String result = "";
		
		result += this.getId();
		result += "/"+this.getFirstName();
		result += "/"+this.getLastName();
		result += "/"+this.getStringHireDate();
		
		return result;
	}
}
