package flashcopy_workrate.view.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import flashcopy_workrate.model.PeopleWorkRate;

public class WeekSumUp {
	private int weekAverage = 0;
	private int requestedAverage = 0;
	private HashMap<String, ArrayList<String>> machinesOfProjects;
	private ArrayList<PeopleWorkRate> workrates;
	private int numberOfDay = 0;
	private HashMap<String, ArrayList<PeopleWorkRate>> workratePerDay;
	public WeekSumUp(ArrayList<PeopleWorkRate> workrates)
	{
		this.workrates = workrates;
		WeekProject weekProjects = new WeekProject();
		workratePerDay = new HashMap<String, ArrayList<PeopleWorkRate>>();
		for(PeopleWorkRate workrate : workrates)
		{
			boolean exist = true;
			ArrayList<PeopleWorkRate> workrateOfDay = workratePerDay.get(workrate.getStringDate());
			if(workrateOfDay == null)
			{
				workrateOfDay = new ArrayList<PeopleWorkRate>();
				exist = false;
			}				
			workrateOfDay.add(workrate);
			if(!exist)
				workratePerDay.put(workrate.getStringDate(), workrateOfDay);
			weekProjects.add(workrate.getCurrentProject().getName(), workrate);
		}
		machinesOfProjects = weekProjects.getMachineOfProject();
		for(Map.Entry<String, ArrayList<PeopleWorkRate>> entry : workratePerDay.entrySet())
		{
			DaySumUp day = new DaySumUp(entry.getValue());
			int rate =  day.getRate();
			int expected = day.getRequestedRate();
			if(rate != 0 && expected != 0)
			{
				numberOfDay ++;
				this.weekAverage += rate;
				this.requestedAverage += expected;
			}
			
		}
	}
	public String getColorValidation()
	{
		if(this.weekAverage == 0)
			return "red";
		if(this.weekAverage >= this.requestedAverage)
			return "green";
		else {
			return "red";
		}
	}
	public int getNumberOfDay()
	{
		return this.numberOfDay;
	}
	public int getWeekAverage()
	{
		return this.weekAverage;
	}
	public int getRequestedRate()
	{
		return this.requestedAverage;
	}
	public ArrayList<String> getProjects()
	{
		ArrayList<String> projects = new ArrayList<String>();
		for(Map.Entry<String, ArrayList<String>> entry :this.machinesOfProjects.entrySet())
		{
			projects.add(entry.getKey());
		}		
		return projects;
	}
	public String getMachineOfProjects(String project)
	{
		ArrayList<String> machines = this.machinesOfProjects.get(project);
		String res = "(";
		for(int i = 0; i < machines.size(); i++)
		{
			String machine = machines.get(i);
			res += machine;
			if(i+1 == machines.size())
				res += ")";
			else
				res += "/";
		}
		return res;
	}
}
