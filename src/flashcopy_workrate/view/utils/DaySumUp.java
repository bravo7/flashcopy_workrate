package flashcopy_workrate.view.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import flashcopy_workrate.controller.ProjectController;
import flashcopy_workrate.model.PeopleWorkRate;

public class DaySumUp {
	
	private int rate = 0 ;
	private int requestedRate = 0;
	private HashMap<String, Integer> sortedWorkRates;
	public DaySumUp(ArrayList<PeopleWorkRate> workrates)
	{
		this.sortedWorkRates = new HashMap<String, Integer>();
		for(PeopleWorkRate rate : workrates)
		{
			boolean hasComment = rate.getComment() != null && !"".equals(rate.getComment());
			if(!hasComment)
			{
				Integer possibleValue = sortedWorkRates.get(rate.getCurrentProject().getName());
				if(possibleValue == null)
					sortedWorkRates.put(rate.getCurrentProject().getName(), rate.getRate());
				else
					sortedWorkRates.put(rate.getCurrentProject().getName(), possibleValue + rate.getRate());
			}			
		}
		for(Map.Entry<String, Integer> entry : this.sortedWorkRates.entrySet())
		{
			String projectName = entry.getKey();
			Integer projectRate = entry.getValue();
			
			requestedRate += ProjectController.getProjectByName(projectName).getAverageWorkRate();
			rate += projectRate;
		}
	}
	public int getRate()
	{
		return this.rate;
	}
	public int getRequestedRate()
	{
		return this.requestedRate;
	}
}
