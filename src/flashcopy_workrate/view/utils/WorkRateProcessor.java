package flashcopy_workrate.view.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import flashcopy_workrate.model.PeopleWorkRate;

public class WorkRateProcessor {
	public static ArrayList<WeekSumUp> getWorkRateByWeek(ArrayList<PeopleWorkRate> workrates, int month, int year)
	{
		ArrayList<WeekSumUp> res = new ArrayList<WeekSumUp>();		
		ArrayList<PeopleWorkRate> currentList = null;
		boolean firstMondayPassed = false;
		for(PeopleWorkRate workRate : workrates)
		{
			if(!isFriday(workRate.getWorkRateDate()))
			{
				boolean isMonday = isMonday(workRate.getWorkRateDate());
				if(isMonday)
				{				
					if(!firstMondayPassed && currentList!=null)
					{
						res.add(new WeekSumUp(currentList));
						currentList = new ArrayList<PeopleWorkRate>();
					}
					if(currentList == null)
						currentList = new ArrayList<PeopleWorkRate>();
					currentList.add(workRate);
					firstMondayPassed = true;
				}
				else
				{
					if(!firstMondayPassed && currentList == null)
					{
						currentList = new ArrayList<PeopleWorkRate>();
					}
					firstMondayPassed = false;
					currentList.add(workRate);
					if(workRate.getStringDate().equals(workrates.get(workrates.size()-1).getStringDate()))
					{
						res.add(new WeekSumUp(currentList));
					}
				}		
			}
			else
			{
				res.add(new WeekSumUp(currentList));
				firstMondayPassed = false;
				currentList = null;
			}
		}
		return res;
	}	
	public static boolean isMonday(Date date)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
        int day = cal.get(Calendar.DAY_OF_WEEK);
        if(day == 2)
        	return true;
        else {
			return false;
		}
	}
	public static boolean isFriday(Date date)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
        int day = cal.get(Calendar.DAY_OF_WEEK);
        if(day == 6)
        	return true;
        else {
			return false;
		}
	}
}
