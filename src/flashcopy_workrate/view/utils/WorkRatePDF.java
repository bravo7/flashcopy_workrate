package flashcopy_workrate.view.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.Style;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Text;

import flashcopy_workrate.model.People;

public class WorkRatePDF {
	private static String folderLocation = System.getProperty("user.dir")+"/pdf";
	private Path currentPath;
	private String currentFile;
	public WorkRatePDF()
	{
		createNewSubFolder();
		new File(this.currentPath.toString()).mkdirs();
		
	}
	public void process(ArrayList<WeekSumUp> month, String monthName, People people) throws FileNotFoundException
	{
		this.currentFile = this.currentPath+"/WorkRate_"+people.getId()+"_"+people.getFullName()+"_"+monthName+".pdf";
		writeIntoFile(month, monthName, people);
	}
	private void writeIntoFile(ArrayList<WeekSumUp> month, String monthName, People people) throws FileNotFoundException
	{
		PdfWriter writer = new PdfWriter(this.currentFile);
		PdfDocument pdfDoc = new PdfDocument(writer);
		Document document = new Document(pdfDoc); 
		Paragraph para = new Paragraph (people.getFullName() + " - "+monthName); 
		document.add(para); 
		
		int weekNumber = 0;
		for(WeekSumUp week : month)
		{
			weekNumber++;
			document.add(new Paragraph ("Semaine numéro : "+weekNumber)); 
			
			for(String project : week.getProjects())
			{
				Paragraph paraProject = new Paragraph("Projet : "+ project +" "+week.getMachineOfProjects(project));
				paraProject.setMarginLeft(25);
				document.add(paraProject);
			}			
			
			Style style = new Style();
			if(week.getColorValidation().equals("red"))
			{
				Color color = new DeviceRgb(255, 0, 0);
				style.setFontColor(color);
			}
			else {
				Color color = new DeviceRgb(0, 128, 0);
				style.setFontColor(color);
			}
			Paragraph paragraphRate = new Paragraph("Moyenne : ");
			paragraphRate.add(new Text(""+(week.getWeekAverage()/ week.getNumberOfDay())).addStyle(style));
			paragraphRate.setMarginLeft(50);
			document.add(paragraphRate);
			
			Paragraph paragraphExp = new Paragraph("Moyenne attendue : "+(week.getRequestedRate()/ week.getNumberOfDay()));
			paragraphExp.setMarginLeft(50);
			document.add(paragraphExp);
		}
		
		document.close();
	}
	private void createNewSubFolder()
	{
		Date date =Calendar.getInstance().getTime();  
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");  
        String strDate = dateFormat.format(date);
        String newPathString = folderLocation+"/SumUp"+strDate;
        this.currentPath = getValidPath(newPathString,0);
        
	}
	private Path getValidPath(String basePath, int index)
	{
		Path resPath = null;
		if(index == 0)
			resPath = Paths.get(basePath);
		else {
			resPath = Paths.get(basePath+"-"+index+"/");
		}
		
		if(Files.exists(resPath))
		{
			return getValidPath(basePath,(index+1) );
		}
		else {
			return resPath;
		}
	}
}