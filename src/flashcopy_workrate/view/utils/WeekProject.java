package flashcopy_workrate.view.utils;

import java.util.ArrayList;
import java.util.HashMap;

import flashcopy_workrate.model.PeopleWorkRate;

public class WeekProject {
	
	private HashMap<String, ArrayList<String>> machinesPerProject;
	public WeekProject()
	{
		this.machinesPerProject = new HashMap<String, ArrayList<String>>();
	}
	public void add(String projectName, PeopleWorkRate rate)
	{
		boolean exist = true;
		ArrayList<String> machineOfProject = this.machinesPerProject.get(projectName);
		if(machineOfProject == null)
		{	
			machineOfProject = new ArrayList<String>();
			exist = false;
		}
		
		String machineName =rate.getMachine().getName();
		if(!machineOfProject.contains(machineName))
			machineOfProject.add(machineName);
		
		if(!exist)
			machinesPerProject.put(projectName, machineOfProject);
			
	}
	public HashMap<String, ArrayList<String>> getMachineOfProject()
	{
		return this.machinesPerProject;
	}
}
