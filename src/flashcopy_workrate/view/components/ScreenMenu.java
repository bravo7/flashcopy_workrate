package flashcopy_workrate.view.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import flashcopy_workrate.model.People;
import flashcopy_workrate.view.components.screenmenu.MachinePanel;
import flashcopy_workrate.view.components.screenmenu.PDFSumUp;
import flashcopy_workrate.view.components.screenmenu.PeoplePanel;
import flashcopy_workrate.view.components.screenmenu.ProjectPanel;
import flashcopy_workrate.view.components.screenmenu.SumUpPanel;
import flashcopy_workrate.view.components.screenmenu.WorkRatePanel;

public class ScreenMenu extends JPanel{
	//properties
	ArrayList<String> menus = null;
	
	//constructors
	public ScreenMenu()
	{
		this.menus = new ArrayList<String>( Arrays.asList("Personne", "Projet", "Machine", "Synthese", "Synthèse PDF") );
		this.setBackground(new Color(222, 231, 247));
		this.setLayout(new BorderLayout());
		this.setBorder(new EmptyBorder(20, 20, 20, 20));
	}
	
	//methods
	public void changeScreenDisplay(int index)
	{
		switch(index) {
			case 0: this.displayPeople();
				break;
			case 1: this.displayProject();
				break;
			case 2: this.displayMachine();
				break;
			case 3 : this.diplaySumUp();
				break;
			case 4 : this.displayPDF();
				break;
		}
	}
	public void changePeople(int peopleId)
	{
		this.clear();
		this.add(new WorkRatePanel(peopleId), BorderLayout.CENTER);
	}
	
	public void clearPanel()
	{
		this.clear();
	}
	private void displayPDF()
	{
		this.clear();
		this.add(new PDFSumUp(), BorderLayout.CENTER);
	}
	private void diplaySumUp()
	{
		this.clear();
		this.add(new SumUpPanel(), BorderLayout.CENTER);
	}
	private void displayMachine()
	{
		this.clear();
		this.add(new MachinePanel(), BorderLayout.CENTER);
	}
	private void displayProject()
	{
		this.clear();
		this.add(new ProjectPanel(), BorderLayout.CENTER);
	}
	private void displayPeople()
	{
		this.clear();
		this.add(new PeoplePanel(), BorderLayout.CENTER);
	}
	private void clear()
	{
		this.removeAll();
		this.revalidate();
		this.repaint();
	}
	
	//accessors
	public ArrayList<String> getMenus()
	{
		return this.menus;
	}
}
