package flashcopy_workrate.view.components.screenmenu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import flashcopy_workrate.Main;
import flashcopy_workrate.controller.PeopleController;
import flashcopy_workrate.controller.WorkRateController;
import flashcopy_workrate.model.People;
import flashcopy_workrate.model.PeopleWorkRate;
import flashcopy_workrate.view.utils.WeekSumUp;
import flashcopy_workrate.view.utils.WorkRateProcessor;

public class SumUpPanel extends JPanel {
	private JComboBox<ComboItem> peopleCombo;
	private JComboBox<ComboItem> monthCombo;
	private GridBagConstraints c;
	private JPanel resultPanel;
	public SumUpPanel()
	{
		JPanel headerJPanel = new JPanel(new GridBagLayout());
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1;
		c.weighty = 1;
		c.insets = new Insets(8, 10, 5, 8);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		headerJPanel.add(new JLabel("Personne : "),c);
		c.gridx++;
		headerJPanel.add(getPeopleCombo(),c);
		c.gridx ++;
		headerJPanel.add(new JLabel("Mois : "),c);
		c.gridx++;
		headerJPanel.add(getMonthCombo(),c);
		this.add(headerJPanel);
		c.gridx = 0;
		c.gridy = 1;
		this.setLayout(new FlowLayout(FlowLayout.LEFT));
		resultPanel = new JPanel();
		resultPanel.setLayout(new BoxLayout(resultPanel, BoxLayout.Y_AXIS));
		this.add(resultPanel);
	}
	private JComboBox<ComboItem> getPeopleCombo()
	{
		JComboBox<ComboItem> res = new JComboBox<ComboItem>();
		res.addItem(new ComboItem(-1, ""));
		res.setPreferredSize(new Dimension(300,(int)res.getPreferredSize().getHeight()));
		ArrayList<People> peoples = PeopleController.getPeoples();
		if(peoples != null)
		{
			for(People people : peoples)
			{
				res.addItem(new ComboItem(people.getId(), people.getFullName()));
			}
		}
		res.setSelectedItem("");
		peopleCombo = res;
		SumUpPanel self = this;
		res.addItemListener(new ItemListener() {
			@Override
		    public void itemStateChanged(ItemEvent event) {
				if (event.getStateChange() == ItemEvent.SELECTED) {
					ComboItem peopleSelected = (ComboItem) ((JComboBox<?>) event.getSource()).getSelectedItem();
					ComboItem monthSelected = (ComboItem) (self.monthCombo.getSelectedItem());
					if(peopleSelected.id != -1 && monthSelected.id != -1)
					{
						self.setValue(peopleSelected.id,monthSelected.id);
					}
				}
		    }    
		});
		
		return res;
	}
	private JComboBox<ComboItem> getMonthCombo()
	{
		JComboBox<ComboItem> res = new JComboBox<ComboItem>();
		res.addItem(new ComboItem(-1,""));
		res.addItem(new ComboItem(1,"Janvier"));
		res.addItem(new ComboItem(2,"Février"));
		res.addItem(new ComboItem(3,"Mars"));
		res.addItem(new ComboItem(4,"Avril"));
		res.addItem(new ComboItem(5,"Mai"));
		res.addItem(new ComboItem(6,"Juin"));
		res.addItem(new ComboItem(7,"Juillet"));
		res.addItem(new ComboItem(8,"Août"));
		res.addItem(new ComboItem(9,"Septembre"));
		res.addItem(new ComboItem(10,"Octobre"));
		res.addItem(new ComboItem(11,"Novembre"));
		res.addItem(new ComboItem(12,"Décembre"));
		SumUpPanel self = this;
		monthCombo = res;
		res.addItemListener(new ItemListener() {
			@Override
		    public void itemStateChanged(ItemEvent event) {
				if (event.getStateChange() == ItemEvent.SELECTED && peopleCombo != null) {
					ComboItem peopleSelected = (ComboItem) peopleCombo.getSelectedItem();
					ComboItem monthSelected = (ComboItem) ((JComboBox<?>) event.getSource()).getSelectedItem();
					if(peopleSelected.id != -1 && monthSelected.id != -1)
					{
						self.setValue(peopleSelected.id,monthSelected.id);
					}
				}
		    }    
		});
		return res;
	}
	private void setValue(int peopleID, int month)
	{
		resultPanel.removeAll();
		int year = Calendar.getInstance().get(Calendar.YEAR);
		ArrayList<PeopleWorkRate> workrates = WorkRateController.getWorkRates(peopleID, month, year);
		
		if(workrates.size() > 0)
		{
			ArrayList<WeekSumUp> weekSumUps = WorkRateProcessor.getWorkRateByWeek(workrates, month,year);			
			int i = 0;
			for(WeekSumUp weekSumUp : weekSumUps)
			{
				i++;
				resultPanel.add(getSumUpComponent(i,weekSumUp));
			}
			
		}
		this.revalidate();
		this.repaint();
	}
	private JPanel getSumUpComponent(int weekNumber ,WeekSumUp weekSumUp)
	{
		JPanel resJPanel = new JPanel(new GridBagLayout());
		GridBagConstraints cons = new GridBagConstraints();
		cons.gridx = 0;
		cons.gridy = 0;
		cons.weightx = 1;
		cons.fill = GridBagConstraints.HORIZONTAL;
		cons.insets = new Insets(8, 10, 5, 8);
		cons.anchor = GridBagConstraints.NORTHWEST;
		
		resJPanel.add(new JLabel("Semaine numéro : "+weekNumber),cons);
		cons.gridy++;
		cons.gridx++;
		for(String projectName : weekSumUp.getProjects())
		{
			resJPanel.add(new JLabel( projectName +" : "+weekSumUp.getMachineOfProjects(projectName)),cons);
			cons.gridy ++;
		}		
		
		String averageString = "<html>Moyenne : <font color="+weekSumUp.getColorValidation()+">"+(weekSumUp.getWeekAverage() / weekSumUp.getNumberOfDay())+"</font></html>";
		resJPanel.add(new JLabel(averageString),cons);
		cons.gridy++;
		resJPanel.add(new JLabel("Attendue : "+(weekSumUp.getRequestedRate() / weekSumUp.getNumberOfDay())),cons);
		return resJPanel;
	}
	private class ComboItem
	{
		private String value;
		private int id;
		public ComboItem (int id, String value)
		{
			this.id = id;
			this.value = value;
		}
		public String toString()
		{
			return this.value;
		}
	}
}
