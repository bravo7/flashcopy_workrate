package flashcopy_workrate.view.components.screenmenu;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.logging.Level;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import flashcopy_workrate.Main;
import flashcopy_workrate.model.People;

public class PeoplePopUp extends JDialog{
	private PeoplePanel owner;
	private boolean modeCreation = true;
	public PeoplePopUp(PeoplePanel owner, People people, int index)
	{
		super();
		this.owner = owner;
		if(people != null)
			modeCreation = false;
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		
		this.setSize(500, 200);
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		this.setContentPane(this.buildContent(people, index));
		this.setResizable(false);
		this.setVisible(true);
	}
	
	public JPanel buildContent(People people, int index)
	{
		PeoplePopUp that = this;
		JPanel content = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1;
		c.insets = new Insets(8, 10, 5, 8);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		content.add(new JLabel("Prénom : "),c);
		c.gridy++;
		content.add(new JLabel("Nom : "),c);
		c.gridy++;
		content.add(new JLabel("Date embauche (JJ/MM/AAAA) : "),c);
		c.gridy = 0;
		c.gridx = 1;
		JTextField jFirstname = new JTextField(13);
		if(!modeCreation)
			jFirstname.setText(people.getFirstName());
		content.add(jFirstname,c);
		c.gridy++;
		JTextField jName = new JTextField(13);
		if(!modeCreation)
			jName.setText(people.getLastName());
		content.add(jName,c);
		c.gridy++;
		JTextField jDate = new JTextField(13);
		if(!modeCreation)
			jDate.setText(people.getStringHireDate());
		content.add(jDate,c);
		
		c.gridy++;
		c.gridx = 1;
		JButton b = new JButton("Ok");
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				try {
					People p;
					String startDate=jDate.getText();
					SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
					java.util.Date date;
					date = sdf1.parse(startDate);
					java.sql.Date sqlDate = new java.sql.Date(date.getTime()); 
					p = new People(jFirstname.getText(), jName.getText(), sqlDate);
					if(that.modeCreation)
						that.owner.setNewPeople(p);
					else
					{
						p.setId(people.getId());
						that.owner.setPeople(p, index);
					}
					that.dispose();
				} catch (ParseException e1) {
					Main.LOGGER.log(Level.SEVERE,e1.getMessage(),e1);
				}
				
			}
		});
		content.add(b,c);
		
		return content;
	}
}
