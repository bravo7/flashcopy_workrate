package flashcopy_workrate.view.components.screenmenu;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import flashcopy_workrate.Main;
import flashcopy_workrate.controller.PeopleController;
import flashcopy_workrate.controller.WorkRateController;
import flashcopy_workrate.model.People;
import flashcopy_workrate.model.PeopleWorkRate;
import flashcopy_workrate.view.utils.WeekSumUp;
import flashcopy_workrate.view.utils.WorkRatePDF;
import flashcopy_workrate.view.utils.WorkRateProcessor;

public class PDFSumUp extends JPanel {
	private GridBagConstraints c;
	private JComboBox<ComboItem> monthsBox;
	public PDFSumUp()
	{
		JPanel headerJPanel = new JPanel(new GridBagLayout());
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1;
		c.weighty = 1;
		c.insets = new Insets(8, 10, 5, 8);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		headerJPanel.add(new JLabel("Mois : "),c);
		c.gridx++;
		headerJPanel.add(getMonthCombo(),c);
		c.gridx++;
		JButton generateButton = new JButton("Générer");
		generateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				ComboItem item = (ComboItem) monthsBox.getSelectedItem();
				if(item.id != -1)
				{
					WorkRatePDF pdf = new WorkRatePDF();
					ArrayList<People> peoples =PeopleController.getPeoples();
					for(People people : peoples)
					{
						int year = Calendar.getInstance().get(Calendar.YEAR);
						ArrayList<PeopleWorkRate> workrates = WorkRateController.getWorkRates(people.getId(), item.id, year);
						ArrayList<WeekSumUp> weekSumUps = WorkRateProcessor.getWorkRateByWeek(workrates,  item.id,year);
						if(weekSumUps.size() > 0)
						{
							try {
								pdf.process(weekSumUps, item.value, people);
							} catch (FileNotFoundException exp) {
								Main.LOGGER.log(Level.SEVERE,exp.getMessage(),exp);
							}
						}						
					}		
				}
			}
		});
		headerJPanel.add(generateButton);
		this.setLayout(new FlowLayout(FlowLayout.LEFT));
		this.add(headerJPanel);
	}
	private JComboBox<ComboItem> getMonthCombo()
	{
		JComboBox<ComboItem> res = new JComboBox<ComboItem>();
		monthsBox = res;
		res.addItem(new ComboItem(-1,""));
		res.addItem(new ComboItem(1,"Janvier"));
		res.addItem(new ComboItem(2,"Février"));
		res.addItem(new ComboItem(3,"Mars"));
		res.addItem(new ComboItem(4,"Avril"));
		res.addItem(new ComboItem(5,"Mai"));
		res.addItem(new ComboItem(6,"Juin"));
		res.addItem(new ComboItem(7,"Juillet"));
		res.addItem(new ComboItem(8,"Août"));
		res.addItem(new ComboItem(9,"Septembre"));
		res.addItem(new ComboItem(10,"Octobre"));
		res.addItem(new ComboItem(11,"Novembre"));
		res.addItem(new ComboItem(12,"Décembre"));
		return res;
	}
	private class ComboItem
	{
		private String value;
		private int id;
		public ComboItem (int id, String value)
		{
			this.id = id;
			this.value = value;
		}
		public String toString()
		{
			return this.value;
		}
	}
}
