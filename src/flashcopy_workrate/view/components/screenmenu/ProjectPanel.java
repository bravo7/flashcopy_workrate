package flashcopy_workrate.view.components.screenmenu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import flashcopy_workrate.controller.MachineController;
import flashcopy_workrate.controller.PeopleController;
import flashcopy_workrate.controller.ProjectController;
import flashcopy_workrate.model.People;
import flashcopy_workrate.model.Project;

public class ProjectPanel extends JPanel{
	private JTable table = null;
	public ProjectPanel()
	{
		ProjectPanel that = this;
		this.setLayout(new BorderLayout());
		this.setBackground(new Color(222, 231, 247));
		ArrayList<Project> projects = ProjectController.getProjects();
		DefaultTableModel model = new DefaultTableModel();
		String[] columns = new String[] {"Nom de projet", "WorkRate moyen"};
		for(String column : columns)
			model.addColumn(column);
		String[] data = new String[2]; 
		for(int i = 0; i < projects.size(); i++)
		{
			data[0] = projects.get(i).getName();
			data[1] = ""+projects.get(i).getAverageWorkRate();
			model.addRow(data);
			
		}
		table = new JTable(model){public boolean isCellEditable(int rowIndex, int colIndex){return false;}};;
		table.setRowSelectionAllowed(true);
		//table.setEnabled(false);
		this.add(new JScrollPane(table));		
		JPanel jpanelButton = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JButton button = new JButton("Nouveau");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				new ProjectPopUp(that);
			}
		});
		JButton removeButton = new JButton("Supprimer");
		removeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				if(table.getSelectedRow() != -1)
				{
					String projectName = table.getValueAt(table.getSelectedRow(), 0).toString();
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					if(	ProjectController.deleteProject(projectName))
						model.removeRow(table.getSelectedRow());
				}
			}
		});
		jpanelButton.add(removeButton);
		jpanelButton.add(button);
		this.add(jpanelButton, BorderLayout.SOUTH);
	}
	public void setNewProject(Project p)
	{
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		if(ProjectController.createProject(p))
			model.addRow(new String[] {p.getName(),""+ p.getAverageWorkRate()});	
	}
}
