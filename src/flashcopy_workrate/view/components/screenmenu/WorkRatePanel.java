package flashcopy_workrate.view.components.screenmenu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.ColorModel;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.DatePickerSettings;
import com.github.lgooddatepicker.components.DatePickerSettings.DateArea;
import com.github.lgooddatepicker.optionalusertools.DateChangeListener;
import com.github.lgooddatepicker.zinternaltools.DateChangeEvent;

import flashcopy_workrate.controller.MachineController;
import flashcopy_workrate.controller.PeopleController;
import flashcopy_workrate.controller.ProjectController;
import flashcopy_workrate.controller.WorkRateController;
import flashcopy_workrate.model.Machine;
import flashcopy_workrate.model.People;
import flashcopy_workrate.model.PeopleWorkRate;
import flashcopy_workrate.model.Project;
import flashcopy_workrate.model.dbaccess.PeopleAccess;
import flashcopy_workrate.model.dbaccess.WorkRateAccess;

public class WorkRatePanel extends JPanel{
	
	private JComboBox projectCombo;
	private JComboBox machineCombo;
	private JTextField jworkRate;
	private JTextField jComment;
	private PeopleWorkRate currentRate;
	private boolean firstLoad = true;
	private int peopleId;
	private JTable table;
	private JComboBox<ComboItem> monthCombo;
	private HashMap<Integer, String> months;
	
	public WorkRatePanel(int peopleId)
	{
		WorkRatePanel self = this;
		self.peopleId = peopleId;
		People people = PeopleController.getPeopleByID(peopleId);
		if(people != null)
		{
			this.setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 0;
			c.weightx = 1;
			c.insets = new Insets(8, 10, 5, 8);
			c.anchor = GridBagConstraints.NORTHWEST;
			this.add(new JLabel("Personne : "),c);
			c.gridy ++;
			this.add(new JLabel("Projet Actuel : "),c);
			c.gridy ++;
			this.add(new JLabel("Machine Actuel : "),c);
			c.gridy ++;
			this.add(new JLabel("Date : "),c);
			c.gridy ++;
			this.add(new JLabel("WorkRate : "),c);
			c.gridy ++;
			this.add(new JLabel("Commentaire : "),c);
			
			c.gridx = 1;
			c.gridy = 0;
			JTextField jPeople = new JTextField(15);
			jPeople.setEditable(false);
			jPeople.setText(people.getFullName());
			this.add(jPeople,c);
			c.gridy ++;
			
			DatePicker datePicker = new DatePicker(new DatePickerSettings(new Locale("fr")));
			projectCombo = getProjectComboBox(people);
			projectCombo.addItemListener(new ItemListener() {
				@Override
			    public void itemStateChanged(ItemEvent event) {
					if(!firstLoad)
					{
						 if (event.getStateChange() == ItemEvent.SELECTED) {
					          String project =(String) event.getItem();
					          self.setValues( java.sql.Date.valueOf(datePicker.getDate()), people.getId(), project, (String) machineCombo.getSelectedItem());
					       }
					}
			      
			    }    
			});
			this.add(projectCombo,c);
			c.gridy ++;
			
			machineCombo = getMachineComboBox(people);
			this.add(machineCombo,c);
			machineCombo.addItemListener(new ItemListener() {
				@Override
			    public void itemStateChanged(ItemEvent event) {
					if(!firstLoad)
					{
						if (event.getStateChange() == ItemEvent.SELECTED) {
					          String machine =(String) event.getItem();
					          self.setValues( java.sql.Date.valueOf(datePicker.getDate()), people.getId(), (String) projectCombo.getSelectedItem(),machine);
				       }
					}			       
			    }    
			});
			c.gridy ++;
			
			
			datePicker.addDateChangeListener( new DateChangeListener() {

				@Override
				public void dateChanged(DateChangeEvent event) {
					java.sql.Date newDate = java.sql.Date.valueOf(event.getNewDate());
					LocalDate oldLocalDate = event.getOldDate();
					if(oldLocalDate != null)
					{
						java.sql.Date oldDate = java.sql.Date.valueOf(oldLocalDate);
						if(!newDate.equals(oldDate))
							self.setValues(newDate, people.getId(), (String) projectCombo.getSelectedItem(), (String) machineCombo.getSelectedItem());
					}
					
				}
			});
			datePicker.setDateToToday();
			this.add(datePicker,c);
			c.gridy++;
			
			jworkRate = new JTextField(15);
			this.add(jworkRate,c);
			c.gridy++;
			
			jComment = new JTextField(15);
			this.add(jComment,c);
			
			c.gridy++;
			c.weighty = 0.6;
			JButton saveButton = new JButton("Sauvegarder");
			saveButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e)
				{
					Machine machine = new Machine((String)machineCombo.getSelectedItem());
					Project project = ProjectController.getProjectByName((String)projectCombo.getSelectedItem());
					LocalDate localDate = datePicker.getDate();
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
					String date = localDate.format(formatter);
					String comment = jComment.getText();
					String rate = jworkRate.getText();
					if((comment == null || "".equals(comment) && (rate == null || "".equals(rate))))
						WorkRateController.deleteWorkRate(date,people.getId(), project.getName(), machine.getName());
					else {
						if(currentRate != null)
							WorkRateController.updateWorkRate(people, machine, project,date, comment, rate);
						else
							WorkRateController.createWorkRate(people, machine, project,date, comment, rate);
					}
				}
			});
			this.add(saveButton,c);
			PeopleWorkRate last = WorkRateController.getLastWorkRate(people);
			if(last != null)
				this.setValues(java.sql.Date.valueOf(datePicker.getDate()), people.getId(),last.getCurrentProject().getName() , last.getMachine().getName());
			c.gridx --;
			c.gridy++;
			this.add(this.getMonthCombo(),c);
			
			table = new JTable();
			DefaultTableModel model = new DefaultTableModel();
			String[] columns = new String[] {"Date","Project", "Machine", "WorkRate"};
			for(String column : columns)
				model.addColumn(column);
			
			table = new JTable(model){public boolean isCellEditable(int rowIndex, int colIndex){return false;}};
			table.setRowSelectionAllowed(true);
			c.gridwidth = 2;
			c.gridy++;
			this.add(new JScrollPane(table),c);
			firstLoad = false;
			this.setTableValue(((ComboItem)monthCombo.getSelectedItem()).id);
		}
	}
	private JComboBox<ComboItem> getMonthCombo()
	{
		if(this.months == null)
		{
			this.months = new HashMap<Integer,String>();;
			months.put(1, "Janvier");
			months.put(2, "Février");
			months.put(3, "Mars");
			months.put(4, "Avril");
			months.put(5, "Mai");
			months.put(6, "Juin");
			months.put(7, "Juillet");
			months.put(8, "Août");
			months.put(9, "Septembre");
			months.put(10, "Octobre");
			months.put(11, "Novembre");
			months.put(12, "Décembre");
		}
		JComboBox<ComboItem> res = new JComboBox<ComboItem>();
		monthCombo = res;
		for(Map.Entry<Integer, String> entry : this.months.entrySet())
		{
			res.addItem(new ComboItem(entry.getKey(), entry.getValue()));
		}	
		java.util.Date date= new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int month = cal.get(Calendar.MONTH);
		month++;
		monthCombo.getModel().setSelectedItem(new ComboItem(month, this.months.get(month)));
		
		WorkRatePanel that = this;
		res.addItemListener(new ItemListener() {
			@Override
		    public void itemStateChanged(ItemEvent event) {
				if (event.getStateChange() == ItemEvent.SELECTED) {
					ComboItem monthSelected = (ComboItem) ((JComboBox<?>) event.getSource()).getSelectedItem();
					that.setTableValue(monthSelected.id);
				}
		    }    
		});
		return res;
	}
	public void setTableValue(int month)
	{		
		java.util.Date date= new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int year = cal.get(Calendar.YEAR);
		
		
		monthCombo.setSelectedItem(new ComboItem(month,this.months.get(month)));
		ArrayList<PeopleWorkRate> result = WorkRateController.getWorkRates(this.peopleId, month, year);
		
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		int rows = model.getRowCount(); 
		for(int i = rows - 1; i >=0; i--)
		{
		   model.removeRow(i); 
		}
		for(PeopleWorkRate rate : result)
		{
			//date, project, machine, rate
			String[] data = new String[4]; 		
			data[0] = ""+rate.getStringDate();
			data[2] = ""+rate.getMachine().getName();
			data[3] = ""+rate.getRate();
			data[1] = ""+ rate.getCurrentProject().getName();
			model.addRow(data);
		}
		
		
	}
	public void setValues(java.sql.Date date, int people, String project, String machine )
	{
		PeopleWorkRate rate = WorkRateController.getWorkRate(date, people,project, machine);
		currentRate = rate;
		if(rate != null)
		{
			if(jComment != null)
				this.jComment.setText(rate.getComment());
			if(jworkRate != null)
				this.jworkRate.setText(String.valueOf(rate.getRate()));
			if(machineCombo != null)
				this.machineCombo.setSelectedItem(rate.getMachine().getName());
			if(projectCombo != null)
				this.projectCombo.setSelectedItem(rate.getCurrentProject().getName());
		}	
		else
		{
			rate = WorkRateController.getLastWorkRate(PeopleController.getPeopleByID(people));
			if(rate != null)
			{
				if(machineCombo != null && "".equals(machineCombo.getSelectedItem()))
					this.machineCombo.setSelectedItem(rate.getMachine().getName());
				if(projectCombo != null && "".equals(projectCombo.getSelectedItem()))
					this.projectCombo.setSelectedItem(rate.getCurrentProject().getName());
				if(jComment != null && !"".equals(rate.getComment()))
					this.jComment.setText("");
				if(jworkRate != null && !"".equals(rate.getRate()))
					this.jworkRate.setText("");
			}
		}
	}
	public JComboBox getMachineComboBox(People p)
	{
		JComboBox res = new JComboBox();
		res.insertItemAt("", 0);
		res.setPreferredSize(new Dimension(300,(int)res.getPreferredSize().getHeight()));
		String[] machines = MachineController.getMachinesToArray();
		if(machines != null)
		{
			for(String machineName : machines)
			{
				res.addItem(machineName);
			}
		}
		res.setSelectedItem("");
		return res;
	}
	public JComboBox getProjectComboBox(People p)
	{
		JComboBox res = new JComboBox();
		res.insertItemAt("", 0);
		res.setPreferredSize(new Dimension(300,(int)res.getPreferredSize().getHeight()));
		String[] projects = ProjectController.getProjectsArray();
		if(projects != null)
		{
			for(String projectName : projects)
			{
				res.addItem(projectName);
			}
		}
		res.setSelectedItem("");
		return res;
	}
	private class ComboItem
	{
		private String value;
		private int id;
		public ComboItem (int id, String value)
		{
			this.id = id;
			this.value = value;
		}
		public String toString()
		{
			return this.value;
		}
	}
}
