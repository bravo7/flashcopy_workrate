package flashcopy_workrate.view.components.screenmenu;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import flashcopy_workrate.model.People;
import flashcopy_workrate.model.Project;

public class ProjectPopUp extends JDialog{
	private ProjectPanel owner;
	public ProjectPopUp(ProjectPanel owner)
	{
		super();
		this.owner = owner;
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		
		this.setSize(500, 200);
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		this.setContentPane(this.buildContent());
		this.setResizable(false);
		this.setVisible(true);
	}
	
	public JPanel buildContent()
	{
		ProjectPopUp that = this;
		JPanel content = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1;
		c.insets = new Insets(8, 10, 5, 8);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.gridy++;
		content.add(new JLabel("Nom : "),c);
		c.gridy++;
		content.add(new JLabel("WorkRate moyen : "),c);
		c.gridy = 1;
		c.gridx = 1;
		JTextField jName = new JTextField(13);
		content.add(jName,c);
		c.gridy++;
		JTextField jWorkRate = new JTextField(13);
		content.add(jWorkRate,c);
		
		c.gridy++;
		c.gridx = 1;
		JButton b = new JButton("Ok");
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				Project p;
				p = new Project(jName.getText(), Integer.parseInt(jWorkRate.getText()));
				that.owner.setNewProject(p);
				that.dispose();
			}
		});
		content.add(b,c);
		
		return content;
	}
}
