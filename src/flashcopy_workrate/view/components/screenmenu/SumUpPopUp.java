package flashcopy_workrate.view.components.screenmenu;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import flashcopy_workrate.model.PeopleWorkRate;

public class SumUpPopUp extends JDialog {
	public SumUpPopUp(ArrayList<PeopleWorkRate> listWorkRate)
	{
		super();
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		
		this.setSize(700, 400);
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		this.setContentPane(this.buildContent(listWorkRate));
		this.setResizable(false);
		this.setVisible(true);
	}
	
	public JPanel buildContent(ArrayList<PeopleWorkRate> listWorkRate)
	{
		JTable table;
		JPanel content = new JPanel(new BorderLayout());
		DefaultTableModel model = new DefaultTableModel();
		String[] columns = new String[] {"Date", "WorkRate","Projet", "Commentaire"};
		for(String column : columns)
			model.addColumn(column);
		String[] data = new String[4]; 
		for(int i = 0; i < listWorkRate.size(); i++)
		{
			data[0] = listWorkRate.get(i).getStringDate();
			data[1] = ""+listWorkRate.get(i).getRate();
			data[2] = listWorkRate.get(i).getCurrentProject().getName();
			data[3] = listWorkRate.get(i).getComment();
			model.addRow(data);
			
		}
		table = new JTable(model);
		table.setEnabled(false);
		content.add(new JScrollPane(table), BorderLayout.CENTER);	
		return content;
	}

}
