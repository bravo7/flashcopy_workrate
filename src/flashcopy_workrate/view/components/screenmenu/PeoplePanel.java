package flashcopy_workrate.view.components.screenmenu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import flashcopy_workrate.Main;
import flashcopy_workrate.controller.MachineController;
import flashcopy_workrate.controller.PeopleController;
import flashcopy_workrate.controller.ProjectController;
import flashcopy_workrate.model.People;
import flashcopy_workrate.model.Project;
import flashcopy_workrate.model.dbaccess.PeopleAccess;

public class PeoplePanel extends JPanel{
	private JTable table = null;
	public PeoplePanel()
	{
		PeoplePanel that = this;
		this.setLayout(new BorderLayout());
		this.setBackground(new Color(222, 231, 247));
		ArrayList<People> peoples = PeopleController.getPeoples();
		DefaultTableModel model = new DefaultTableModel();
		String[] columns = new String[] {"Identifiant","Prénom", "Nom", "Date d'embauche", "Projet courant"};
		for(String column : columns)
			model.addColumn(column);
		String[] data = new String[5]; 
		for(int i = 0; i < peoples.size(); i++)
		{
			data[0] = ""+peoples.get(i).getId();
			data[1] = peoples.get(i).getFirstName();
			data[2] = peoples.get(i).getLastName();
			data[3] = peoples.get(i).getStringHireDate();
			Project p = ProjectController.getPeopleCurrentProject(peoples.get(i));
			if(p == null)
				data[4] = "";
			else
				data[4] = p.getName();
			model.addRow(data);
			
		}
		table = new JTable(model){public boolean isCellEditable(int rowIndex, int colIndex){return false;}};
		table.setRowSelectionAllowed(true);
		table.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent event) {
				if (event.getClickCount() == 2) {
				      JTable target = (JTable)event.getSource();
				      int row = target.getSelectedRow();
				      int id = Integer.parseInt(table.getValueAt(table.getSelectedRow(), 0).toString());
				      String firstName = table.getValueAt(table.getSelectedRow(), 1).toString();
				      String lastName = table.getValueAt(table.getSelectedRow(), 2).toString();
				      String date = table.getValueAt(table.getSelectedRow(), 3).toString();
					  SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
				 	  java.sql.Date sqlDate;
					try {
						sqlDate = new java.sql.Date(sdf1.parse(date).getTime());
						People people = new People(firstName, lastName, sqlDate);
						people.setId(id);
					    new PeoplePopUp(that, people, row);
					} catch (ParseException e) {
						Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
					} 
				      
				}
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {}
			@Override
			public void mouseExited(MouseEvent arg0) {}
			@Override
			public void mousePressed(MouseEvent arg0) {}
			@Override
			public void mouseReleased(MouseEvent arg0) {}
		});
		this.add(new JScrollPane(table));		
		JPanel jpanelButton = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JButton button = new JButton("Nouveau");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				new PeoplePopUp(that, null, -1);
			}
		});
		JButton removeButton = new JButton("Supprimer");
		removeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				if(table.getSelectedRow() != -1)
				{
					String peopleID = table.getValueAt(table.getSelectedRow(), 0).toString();
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					if(	PeopleController.deletePeople(Integer.parseInt(peopleID)))
						model.removeRow(table.getSelectedRow());
				}
			}
		});
		jpanelButton.add(removeButton);
		jpanelButton.add(button);
		this.add(jpanelButton, BorderLayout.SOUTH);
	}
	private void refresh()
	{
		DefaultTableModel model = (DefaultTableModel)table.getModel(); 
		int rows = model.getRowCount(); 
		for(int i = rows - 1; i >=0; i--)
		{
		   model.removeRow(i); 
		}
		ArrayList<People> peoples = PeopleController.getPeoples();
		String[] data = new String[5]; 
		for(int i = 0; i < peoples.size(); i++)
		{
			data[0] = ""+peoples.get(i).getId();
			data[1] = peoples.get(i).getFirstName();
			data[2] = peoples.get(i).getLastName();
			data[3] = peoples.get(i).getStringHireDate();
			Project p = ProjectController.getPeopleCurrentProject(peoples.get(i));
			if(p == null)
				data[4] = "";
			else
				data[4] = p.getName();
			model.addRow(data);
			
		}
	}
	public void setPeople(People p,int index)
	{
		Boolean hasBeenCreated = PeopleController.editPeople(p);
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		String[] data = new String[5]; 		
		data[0] = ""+p.getId();
		data[1] = p.getFirstName();
		data[2] = p.getLastName();
		data[3] = p.getStringHireDate();
		Project project = ProjectController.getPeopleCurrentProject(p);
		if(project == null)
			data[4] = "";
		else
			data[4] = project.getName();
		for(int i = 0; i<5;i++)
		{
			model.setValueAt(data[i], index, i);
		}
		
		if(hasBeenCreated)
			refresh();
	}
	public void setNewPeople(People p)
	{
		Boolean hasBeenCreated = PeopleController.createPeople(p);
		if(hasBeenCreated)
			refresh();
		
	}
}
