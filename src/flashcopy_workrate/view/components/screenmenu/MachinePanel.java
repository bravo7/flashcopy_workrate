package flashcopy_workrate.view.components.screenmenu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import flashcopy_workrate.controller.MachineController;
import flashcopy_workrate.controller.ProjectController;
import flashcopy_workrate.model.Machine;
import flashcopy_workrate.model.Project;

public class MachinePanel extends JPanel{
	private JTable table = null;
	public MachinePanel()
	{
		MachinePanel that = this;
		this.setLayout(new BorderLayout());
		this.setBackground(new Color(222, 231, 247));
		ArrayList<Machine> machines = MachineController.getMachines();
		DefaultTableModel model = new DefaultTableModel();
		String[] columns = new String[] {"Nom de machine"};
		for(String column : columns)
			model.addColumn(column);
		String[] data = new String[1]; 
		for(int i = 0; i < machines.size(); i++)
		{
			data[0] = machines.get(i).getName();
			model.addRow(data);
		}
		table = new JTable(model){public boolean isCellEditable(int rowIndex, int colIndex){return false;}};;
		table.setRowSelectionAllowed(true);
		//table.setEnabled(false);
		this.add(new JScrollPane(table));		
		JPanel jpanelButton = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JButton newButton = new JButton("Nouveau");
		newButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				new MachinePopUp(that);
			}
		});
		JButton removeButton = new JButton("Supprimer");
		removeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				if(table.getSelectedRow() != -1)
				{
					String machineName = table.getValueAt(table.getSelectedRow(), 0).toString();
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					if(	MachineController.deleteMachine(machineName))
						model.removeRow(table.getSelectedRow());
				}
			}
		});
		jpanelButton.add(newButton);
		jpanelButton.add(removeButton);
		this.add(jpanelButton, BorderLayout.SOUTH);
	}
	public void setNewMachine(Machine m)
	{
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		if(MachineController.createMachine(m))
			model.addRow(new String[] {m.getName()});	
		if(table != null)
			table.repaint();
	}
}
