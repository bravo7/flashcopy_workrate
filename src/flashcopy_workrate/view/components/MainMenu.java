package flashcopy_workrate.view.components;

import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import flashcopy_workrate.model.People;

public class MainMenu extends JPanel{
	
	//private
	private TabMenu tabMenu = null;
	private TreeMenu treeMenu = null;
	private boolean isInAdminMenu = false;
	private ScreenMenu screen = null;
	private int screenIndex = -1;
	
	//constructor
	public MainMenu(ScreenMenu screen)
	{
		this.screen = screen;
		treeMenu = new TreeMenu(this);
		tabMenu =  new TabMenu(this);
		this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		tabMenu.setMaximumSize(new Dimension(Integer.MAX_VALUE,110));
		this.add(tabMenu);
		this.add(new JScrollPane(treeMenu));
	}
	
	//accessors
	public void setIsInAdminMenu(boolean value)
	{
		this.isInAdminMenu = value;
		changeDisplayOfTree();
	}
	public ArrayList<String> getAdminMenus()
	{
		return this.screen.getMenus();
	}
	public void setScreenIndex(int value)
	{
		this.screenIndex = value;
		screen.changeScreenDisplay(this.screenIndex);
	}
	public void setScreenPeople(int peopleId)
	{
		screen.changePeople(peopleId);
	}
	
	//methods
	private void changeDisplayOfTree()
	{
		this.screen.clearPanel();
		treeMenu.changeDisplay(this.isInAdminMenu);
	}
}
