package flashcopy_workrate.view.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import flashcopy_workrate.controller.PeopleController;
import flashcopy_workrate.model.People;
import flashcopy_workrate.model.dbaccess.PeopleAccess;

public class TreeMenu extends JPanel {
	//properties
	private Color backgroundColor = new Color(55, 64, 79);
	private MainMenu menu = null;
	//constructor
	public TreeMenu(MainMenu menu)
	{
		this.menu = menu;
		this.setBackground(backgroundColor);
		this.setLayout(new BorderLayout());	
		this.setBorder(new EmptyBorder(15, 20, 5, 10) );
		displayWorkRateMenu();
	}
	
	//methods 
	public void changeDisplay(boolean isInAdminMenu)
	{
		if(isInAdminMenu)
			displayAdminMenu();
		else
			displayWorkRateMenu();
			
	}
	public void clear()
	{
		this.removeAll();
		this.revalidate();
		this.repaint();
	}
	private void displayAdminMenu()
	{
		this.clear();
		JList list = new JList(menu.getAdminMenus().toArray());
		list.setForeground(Color.WHITE);
		list.setBackground(backgroundColor);
		list.setFixedCellHeight(35);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		Font f = list.getFont();
		list.setFont(new Font(f.getFamily(),Font.BOLD, 17));
		list.addListSelectionListener(new ListSelectionListener() {
		      public void valueChanged(ListSelectionEvent evt) {
		    	  if(list.getValueIsAdjusting())
		    		  menu.setScreenIndex(list.getSelectedIndex());
		        }
		});
		this.add(list );
	}
	private void displayWorkRateMenu()
	{	
		this.clear();
		CustomList customList = new CustomList(PeopleController.getPeoples());
		JList list = new JList(customList.getPeopleString().toArray());
		list.setForeground(Color.WHITE);
		list.setBackground(backgroundColor);
		list.setFixedCellHeight(35);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		Font f = list.getFont();
		list.setFont(new Font(f.getFamily(),Font.BOLD, 17));
		list.addListSelectionListener(new ListSelectionListener() {
		      public void valueChanged(ListSelectionEvent evt) {
		    	  if(list.getValueIsAdjusting())
		    	  {
		    		  int id = list.getSelectedIndex();
		    		  int realID = customList.getIndexInDb(id);
		    		  menu.setScreenPeople(realID);
		    		  
		    	  }    		
		    		  
		        }
        });
		this.add(list);
	}
	private class CustomList
	{
		private ArrayList<String> listOFPeopleString;
		public CustomList(ArrayList<People> list)
		{
			ArrayList<People> listOfPeopleInDB = list;
			listOFPeopleString = new ArrayList<String>();
			if(listOfPeopleInDB.size() > 0)
			{
				for(People people : listOfPeopleInDB)
				{
					listOFPeopleString.add(people.getId()+"/"+people.getFirstName() + " "+people.getLastName());
				}
			}
		}
		public ArrayList<String> getPeopleString()
		{
			ArrayList<String> finalList = new ArrayList<String>();
			for(String s : listOFPeopleString)
			{
				String[] split = s.split("/");
				if(split != null && split.length > 1)
					finalList.add(split[1]);
			}
			
			return finalList;
		}
		public int getIndexInDb(int idString)
		{
			String[] result = listOFPeopleString.get(idString).split("/");
			return Integer.parseInt(result[0]);	
		}
	}
}
