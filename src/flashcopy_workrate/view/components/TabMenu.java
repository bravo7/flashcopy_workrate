package flashcopy_workrate.view.components;

import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.Border;

import flashcopy_workrate.view.components.tabmenu.AdminButton;
import flashcopy_workrate.view.components.tabmenu.WorkRateButton;

import javax.swing.border.*;

public class TabMenu extends JPanel {
	
	//properties
	private AdminButton adminButton = null;
	private WorkRateButton workRateButton = null;
	private MainMenu mainMenu = null;
	
	//constructors
	public TabMenu(MainMenu mainMenu)
	{
		this.mainMenu = mainMenu;
		this.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT );
		this.setLayout(new FlowLayout());
		adminButton = new AdminButton(this);
		workRateButton = new WorkRateButton(this);
		this.add(adminButton);
		this.add(workRateButton);
		this.setBackground(new Color(71, 85, 109));
	}
	
	//methodes
	public void setAdminActif()
	{
		adminButton.setIsActive(true);
		workRateButton.setIsActive(false);
		mainMenu.setIsInAdminMenu(true);
	}
	public void setWorkRateActif()
	{
		adminButton.setIsActive(false);
		workRateButton.setIsActive(true);
		mainMenu.setIsInAdminMenu(false);
	}
	public Dimension getPreferredSizeButton()
	{
		return adminButton.getPreferredSize();
	}
	public Dimension getMinimumSizeButton()
	{
		return adminButton.getMinimumSize();
	}
	
}
