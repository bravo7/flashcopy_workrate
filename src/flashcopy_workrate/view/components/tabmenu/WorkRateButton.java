package flashcopy_workrate.view.components.tabmenu;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import flashcopy_workrate.view.components.TabMenu;

public class WorkRateButton extends JButton{
	
	//properties
	private boolean isActive = true;
	private Color pressedBackgroundColor = new Color(182, 208, 249);
	private Color backgroundColor = new Color(213, 220, 232);
	private TabMenu tab = null;
	
	//constructor
	public WorkRateButton(TabMenu tab)
	{
		this.tab = tab;
		this.setText("WorkRate");
		this.setFocusPainted(false);
		this.setForeground(Color.BLACK);
		this.setBackground(pressedBackgroundColor);
		this.setPreferredSize(tab.getPreferredSizeButton());
		this.setMinimumSize(tab.getMinimumSizeButton());
		Font f = this.getFont();
		this.setFont(new Font(f.getFamily(),Font.BOLD, 16));
		Border line = new LineBorder(new Color(182, 208, 249));
		Border margin = new EmptyBorder(10,10,10,10);
		Border compound = new CompoundBorder(line, margin);
		this.setBorder(compound);
		this.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				if(!isActive)
					tab.setWorkRateActif();
			}
		});
	}	
	
	//accessors
	public boolean getIsActive()
	{
		return this.isActive;
	}
	public void setIsActive(boolean value)
	{
		this.isActive = value;
		if(value)
			this.setBackground(this.pressedBackgroundColor);
		else
			this.setBackground(this.backgroundColor);
	}
}
