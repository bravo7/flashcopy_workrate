package flashcopy_workrate.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.*;

import flashcopy_workrate.view.components.MainMenu;
import flashcopy_workrate.view.components.ScreenMenu;
import flashcopy_workrate.view.components.TabMenu;
import flashcopy_workrate.view.components.TreeMenu;

public class MainContent extends JPanel{
	public MainContent()
	{
		this.setLayout(new BorderLayout());
		ScreenMenu screen = new ScreenMenu();
		this.add(new MainMenu(screen),BorderLayout.WEST);
		this.add(screen, BorderLayout.CENTER);
		this.setBackground(new Color(0,0,0));
	}
}
