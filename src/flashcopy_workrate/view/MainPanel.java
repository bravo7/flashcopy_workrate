package flashcopy_workrate.view;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.logging.Level;

import javax.swing.JFrame;

import flashcopy_workrate.Main;
import flashcopy_workrate.model.dbaccess.utils.DBConnectionWorkRate;

public class MainPanel extends JFrame{
	public MainPanel()
	{
		super("FlashCopy - WorkRate");
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		
		this.setSize(1200, 900);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
		  public void windowClosing(WindowEvent we) {
				try {
					DBConnectionWorkRate.closeConnection();
				} catch (SQLException e) {
					Main.LOGGER.log(Level.SEVERE,e.getMessage(),e);
				}
		  }
		});
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		this.setContentPane(new MainContent());
		this.setResizable(false);
		this.setVisible(true);
	}
}
