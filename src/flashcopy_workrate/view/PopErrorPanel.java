package flashcopy_workrate.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import flashcopy_workrate.view.components.MainMenu;
import flashcopy_workrate.view.components.ScreenMenu;

public class PopErrorPanel extends JFrame{
	public PopErrorPanel(String message)
	{
		super("FlashCopy - WorkRate - Error");
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		
		this.setSize(400, 300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		this.setContentPane(buildContent(message));
		this.setResizable(false);
		this.setVisible(true);
	}
	private JPanel buildContent(String message)
	{
		JPanel res = new JPanel(new FlowLayout(FlowLayout.LEFT));	
		JTextArea jTextArea = new JTextArea();
		jTextArea.setMinimumSize(new Dimension(400,300));
		jTextArea.setWrapStyleWord(true);
		jTextArea.setLineWrap(true);
		jTextArea.setEditable(false);
		jTextArea.setMinimumSize(new Dimension(Integer.MAX_VALUE,110));
		jTextArea.setText(message);
		res.add(jTextArea, BorderLayout.CENTER);	
		return res;
	}
}
