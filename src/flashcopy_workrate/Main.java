package flashcopy_workrate;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Date;
import java.sql.SQLException;

import flashcopy_workrate.model.Machine;
import flashcopy_workrate.model.People;
import flashcopy_workrate.model.Project;
import flashcopy_workrate.model.dbaccess.MachineAccess;
import flashcopy_workrate.model.dbaccess.PeopleAccess;
import flashcopy_workrate.model.dbaccess.ProjectAccess;
import flashcopy_workrate.model.dbaccess.utils.DBConnectionWorkRate;
import flashcopy_workrate.view.MainPanel;
import flashcopy_workrate.view.PopErrorPanel;
import flashcopy_workrate.view.utils.WorkRatePDF;

import java.util.ArrayList;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Main {

	public static Logger LOGGER = null;
	public static void main(String[] args) {
				
		try {
			LOGGER = Logger.getLogger(Main.class.getName());
			FileHandler fileTxt = new FileHandler(System.getProperty("user.dir")+"/log.out");
			SimpleFormatter formatterTxt = new SimpleFormatter();
	        fileTxt.setFormatter(formatterTxt);
	        LOGGER.addHandler(fileTxt);			
	        
			DBConnectionWorkRate.establishConnection();
			new MainPanel();
			
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE,e.getMessage(),e);
		}
	}

}
